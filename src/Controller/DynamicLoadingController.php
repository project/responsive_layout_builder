<?php

namespace Drupal\responsive_layout_builder\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines DynamicLoadingController class.
 */
class DynamicLoadingController extends ControllerBase {


  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The context repository
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   *
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   */
  public function __construct(Request $current_request, ContextRepositoryInterface $context_repository) {
    $this->currentRequest = $current_request;
    $this->contextRepository = $context_repository;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('context.repository')
    );
  }


  /**
   * Process Ajax request to load components dynamically.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Return ajax response.
   */
  public function content() {

    $components_data = $this->currentRequest->get('components_data');
    // Set current path to path of original page that requested the blocks over Ajax.
    \Drupal::service('path.current')->setPath($this->currentRequest->get('current_path'));

    $response = new AjaxResponse();
    if ($components_data) {
      $components = [];
      foreach ($components_data as $component_data) {
        $component_render_array = $this->getComponent($component_data);
        $response->addCommand(new ReplaceCommand('#' . $component_data['html_element_id'], $component_render_array));
      }
    }
    return $response;
  }

  /**
   * Get render array for a component with given parameters.
   */
  protected function getComponent($component_data) {
    list($entity_type, $entity_id) = explode('.', $component_data['entity_id']);

    $entity = \Drupal::service('entity_type.manager')->getStorage($entity_type)->load($entity_id);
    $context_repository = $this->contextRepository;
    $lbvd = LayoutBuilderEntityViewDisplay::load($component_data['display_id']);

    $type_id = $lbvd->getEntityTypeId();

    // From LayoutBuilderEntityViewDisplay::getContextsForEntity()
    $contexts = [
      'rlb_dynamic_loading' => new Context(ContextDefinition::create('boolean'), TRUE),
        'view_mode' => new Context(ContextDefinition::create('string'), $lbvd->getMode()),
        'entity' => EntityContext::fromEntity($entity),
        'display' => EntityContext::fromEntity($lbvd),
      ] + $context_repository->getAvailableContexts();
    // From LayoutBuilderEntityViewDisplay::buildSections()
    $label = new TranslatableMarkup('@entity being viewed', [
    '@entity' => $entity->getEntityType()->getSingularLabel(),
    ]);

    $contexts['layout_builder.entity'] = EntityContext::fromEntity($entity, $label);

    $section_storage_manager = \Drupal::service('plugin.manager.layout_builder.section_storage');

    // From LayoutBuilderEntityViewDisplay::buildSections()
    $cacheability = new CacheableMetadata();
    $storage = $section_storage_manager->findByContext($contexts, $cacheability);

    $sections = $storage->getSections();

    // Iterate over all sections to find the component by uuid.
    $default_section = $sections[0];
    foreach ($sections as $section) {
      $section_components = $section->getComponents();
      if (isset($section_components[$component_data['component_uuid']])) {
        $component = $section_components[$component_data['component_uuid']];
        return $component->toRenderArray($contexts);
        break;
      }
    }

    // TODO: Need to return something else?
    return [];
  }

}
