<?php

namespace Drupal\responsive_layout_builder\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\Form\ConfigureBlockFormBase;
use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;

/**
 * Provides a form to update a block.
 *
 * @internal
 *   Form classes are internal.
 */
class LayoutBlockSettingsForm extends ConfigureBlockFormBase {

  use LayoutBuilderHighlightTrait;

  /**
   * The breakpoint manager.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected $breakpointManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository'),
      $container->get('context.repository'),
      $container->get('plugin.manager.block'),
      $container->get('uuid'),
      $container->get('plugin_form.factory'),
      $container->get('breakpoint.manager')
    );
  }

  /**
   * Constructs a new block form.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore repository.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID generator.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_manager
   *   The plugin form manager.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository, ContextRepositoryInterface $context_repository, BlockManagerInterface $block_manager, UuidInterface $uuid, PluginFormFactoryInterface $plugin_form_manager, BreakpointManagerInterface $breakpoint_manager) {
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
    $this->contextRepository = $context_repository;
    $this->blockManager = $block_manager;
    $this->uuidGenerator = $uuid;
    $this->pluginFormFactory = $plugin_form_manager;
    $this->breakpointManager = $breakpoint_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_block_responsive_settings_form';
  }

  public function getBreakpointOptionsForGroup($group) {
    $breakpoints = $this->breakpointManager->getBreakpointsByGroup($group);

    $breakpoint_options = [];
    foreach ($breakpoints as $id => $breakpoint) {
      $breakpoint_options[$id] = $breakpoint->getLabel();
    }

    return $breakpoint_options;
  }

  /**
   * Builds the block form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage being configured.
   * @param int $delta
   *   The delta of the section.
   * @param string $region
   *   The region of the block.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return array
   *   The form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $uuid = NULL) {
    $component = $section_storage->getSection($delta)->getComponent($uuid);

    $this->sectionStorage = $section_storage;
    $this->delta = $delta;
    $this->uuid = $component->getUuid();
    $this->block = $component->getPlugin();

    $form['rlb_breakpoint_group'] = [
      '#type' => 'radios',
      '#title' => $this->t('Breakpoint group'),
      '#options' => $this->breakpointManager->getGroups(),
      '#default_value' => $component->get('rlb_breakpoint_group'),
      '#field_prefix' => $this->t('<p>Please make sure to choose a breakpoint group having breakpoints not overlapping each other.</p>'
        . '<p>Overlapping breakpoints may give you surprises in output. Blocks may get hidden in unwanted situations if breakpoints are overlapping.</p>'),
      '#ajax' => [
        'callback' => '::changeBreakpointGroupAjaxCallback',
        'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
        'event' => 'change',
        'wrapper' => 'breakpoints-wrapper', // This element is updated with this AJAX callback.
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Getting breakpoints for selected group...'),
        ],
      ]
    ];

    $breakpoint_options = [];
    if ($selected_breakpoint_group = $form_state->getValue('rlb_breakpoint_group')) {
      $breakpoint_options = $this->getBreakpointOptionsForGroup($selected_breakpoint_group);
      $breakpoints_value = array_keys($breakpoint_options);
      $input = $form_state->getUserInput();
      unset($input['rlb_breakpoints']);
      $form_state->setUserInput($input);
    }
    else if ($selected_breakpoint_group = $component->get('rlb_breakpoint_group')) {
      $breakpoint_options = $this->getBreakpointOptionsForGroup($selected_breakpoint_group);
      $breakpoints_value = $component->get('rlb_breakpoints');
    }

    if ($selected_breakpoint_group) {
      $form['rlb_breakpoints'] = [
        '#title' => $this->t('Breakpoints'),
        '#description' => $this->t('Select one or more breakpoints to display the block in those breakpoints.'),
        '#type' => 'checkboxes',
        '#options' => $breakpoint_options,
        '#default_value' => $breakpoints_value,
        '#prefix' => '<div id="breakpoints-wrapper">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $form['rlb_breakpoints'] = [
        '#prefix' => '<div id="breakpoints-wrapper"></div>',
      ];
    }

    $form['rlb_load_hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load always'),
      '#description' => $this->t('<p>Normally the content for a breakpoint enabled block will not be loaded along with the page because Responsive Layout Builder module will take care of loading the block content via Ajax as necessary for a given screen width.</p>'
        . '<p>However, that block content will then not be available for search engines to index or be visible to other devices where a screen width technically does not exist.</p>'
        . '<p>Therefore, you should enable “Load Always” for any block content that is important to be indexed by search engines.</p>'
        . '<p>If you have multiple instances of the same or very similar content in blocks displayed at different breakpoints, then make sure only one block is enabled so only 1 version of the content displayed to the search engines for indexing.</p>'
        . '<p>If you have only 1 instance of block content that is displayed for some breakpoints, then you should enable this option if you want that content to be indexed by search engines.</p>'
        . 'Note: This checkbox is disabled by default.'),
      '#default_value' => $component->get('rlb_load_hidden'),
    ];

    //  Took from ConfigureBlockFormBase::doBuildForm()
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->submitLabel(),
      '#button_type' => 'primary',
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
      // @todo static::ajaxSubmit() requires data-drupal-selector to be the same
      //   between the various Ajax requests. A bug in
      //   \Drupal\Core\Form\FormBuilder prevents that from happening unless
      //   $form['#id'] is also the same. Normally, #id is set to a unique HTML
      //   ID via Html::getUniqueId(), but here we bypass that in order to work
      //   around the data-drupal-selector bug. This is okay so long as we
      //   assume that this form only ever occurs once on a page. Remove this
      //   workaround in https://www.drupal.org/node/2897377.
      $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    }

    return $form;
  }

  public function changeBreakpointGroupAjaxCallback(array &$form, FormStateInterface $form_state) {
    // Return the prepared breakpoint checkboxes.
    return $form['rlb_breakpoints'];
  }

  /**
   * {@inheritdoc}
   */
  protected function submitLabel() {
    return $this->t('Save');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $section = $this->sectionStorage->getSection($this->delta);
    $component = $section->getComponent($this->uuid);

    $component->set('rlb_breakpoint_group', $form_state->getValue('rlb_breakpoint_group'));
    $component->set('rlb_breakpoints', array_filter(array_values($form_state->getValue('rlb_breakpoints'))));
    $component->set('rlb_load_hidden', $form_state->getValue('rlb_load_hidden'));

    $this->layoutTempstoreRepository->set($this->sectionStorage);
    $form_state->setRedirectUrl($this->sectionStorage->getLayoutBuilderUrl());
  }

}
