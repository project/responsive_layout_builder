<?php

namespace Drupal\responsive_layout_builder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure example settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'responsive_layout_builder.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'responsive_layout_builder_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['load_hidden_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Load hidden for roles'),
      '#options' => static::getRoleOptions(),
      '#default_value' => $config->get('load_hidden_roles'),
      '#description' => $this->t('If selected, content will be loaded for selected roles, but hidden via css for layout purposes.'),
    ];

    // $form['reload'] = [
    //   '#type' => 'checkbox',
    //   '#title' => $this->t('Auto reload'),
    //   '#default_value' => $config->get('reload'),
    //   '#description' => $this->t('If selected, content will be refreshed on resize if media queries change.  This will annoy users but may be handy for development.'),
    // ];

    return parent::buildForm($form, $form_state);
  }

  public static function getRoleOptions() {
    $roles = Role::loadMultiple();
    $role_options = [];
    foreach ($roles as $rid => $role) {
      $role_options[$rid] = $role->label();
    }
    return $role_options;
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('load_hidden_roles', $form_state->getValue('load_hidden_roles'))
      // ->set('reload', $form_state->getValue('reload'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
