<?php

namespace Drupal\responsive_layout_builder\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\breakpoint\BreakpointManagerInterface;
use \Drupal\Core\StringTranslation\StringTranslationTrait;
use \Drupal\Core\StringTranslation\TranslationInterface;
use \Drupal\Core\Session\AccountInterface;

/**
 * Class BlockComponentRenderArraySubscriber.
 */
class BlockComponentRenderArraySubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The breakpoint manager.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected $breakpointManager;

  /**
   * BlockComponentRenderArraySubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Access configuration.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which view access should be checked.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $config_factory, BreakpointManagerInterface $breakpoint_manager, TranslationInterface $translation, AccountInterface $account) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
    $this->breakpointManager = $breakpoint_manager;
    $this->stringTranslation = $translation;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Layout Builder also subscribes to this event to build the initial render
    // array. We use a higher weight so that we execute after it.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 50];
    return $events;
  }

  /**
   * Add each component's block styles to the render array.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $build = $event->getBuild();
    // This shouldn't happen - Layout Builder should have already created the
    // initial build data.
    if (empty($build)) {
      return;
    }

    $breakpoint_group = $event->getComponent()->get('rlb_breakpoint_group');
    if ($breakpoint_group) {
      $contexts = $event->getContexts();

      // This context is set by DynamicLoadingController::content() to help
      // to differentiate dynamically loaded components.
      $dynamic_loading = isset($contexts['rlb_dynamic_loading']);

      $config = $this->configFactory->get('responsive_layout_builder.settings');
      $load_hidden_roles = array_filter($config->get('load_hidden_roles'));
      $user_roles = $this->account->getRoles();
      // This indicates whether the component to be load hidden for current user.
      $load_hidden = !empty(array_intersect_key($load_hidden_roles, array_flip($user_roles)));

      $load_hidden =  $load_hidden || (boolean) $event->getComponent()->get('rlb_load_hidden');

      $breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);
      $breakpoints_selected = array_intersect_key(
        $breakpoints,
        array_flip(array_filter($event->getComponent()->get('rlb_breakpoints')))
      );

      $breakpoints_not_selected = array_diff_key($breakpoints, $breakpoints_selected);
      if (empty($breakpoints_not_selected)) {
        // User has selected all breakpoints. So nothing to do for this component.
        return;
      }

      // Pass all breakpoint media queries to JS side.
      $js_settings_breakpoints = [];
      foreach ($breakpoints as $id => $breakpoint) {
        $js_settings_breakpoints[$id]['mediaquery'] = $breakpoint->getMediaQuery();
      }

      if (!isset($build['#attributes']['class']) || !is_array($build['#attributes']['class'])) {
        $build['#attributes']['class'] = [];
      }

      if (!$event->inPreview()) {
        $classes = [];
        // Prepare classes to hide the block for breakpoints user not selected.
        foreach ($breakpoints_not_selected as $id => $breakpoint) {
          $classes[] = 'hide--' . str_replace('.', '-', $id);
        }

        if ($load_hidden) {
          $classes[] = 'rlb-load-hidden';
        }

        $build['#attributes']['class'] = array_merge($build['#attributes']['class'], $classes);
        $build['#attributes']['data-rlb-load-for-breakpoints'] = implode(',', array_keys($breakpoints_selected));

        if (!empty($breakpoints_not_selected) && !$dynamic_loading && !$load_hidden) {
          $build['content'] = [
            '#markup' => '<div class="rlb-placeholder">Loading...</div>',
          ];
          $build['#attributes']['data-rlb-to-be-loaded'] = "1";
        }
      }

      $build['#attributes']['data-breakpoint-group'] = $breakpoint_group;

      // Set data attributes. It will be used to load component over Ajax.
      $entity = $contexts['layout_builder.entity']->getContextValue();
      $build['#attributes']['data-entity-id'] = $entity->getEntityTypeId() . '.' . $entity->id();

      if (isset($contexts['display'])) {
        $build['#attributes']['data-display-id'] = $contexts['display']->getContextValue()->id();
      }
      $build['#attributes']['data-component-uuid'] = $event->getComponent()->getUuid();

      $build['#attached']['library'][] = 'responsive_layout_builder/rlb';
      $build['#attached']['drupalSettings']['rlb']['breakpoints'] = $js_settings_breakpoints;

      if ($event->inPreview()) {
        if (!empty($breakpoints_not_selected)) {
          // Ensure component is configured to hide at least in one breakpoint.
          if (empty($build['content']['#prefix'])) {
            $build['content']['#prefix'] = '';
          }
          $breakpoint_labels = [];
          foreach ($breakpoints_not_selected as $breakpoint) {
            $breakpoint_labels[] = $breakpoint->getLabel();
          }
          // Show responsive settings hint at top of component.
          $build['content']['#prefix'] .= '<div class="rlb-block-preview">' . $this->t('Hidden for breakpoints in group "@group": @breakpoints', ['@group' => $breakpoint_group, '@breakpoints' => implode(', ', $breakpoint_labels) ]) . '</div>';
          $build['#attached']['library'][] = 'responsive_layout_builder/rlb-admin';
          $build['#attributes']['data-rlb-hide-for-breakpoint-labels'] = $breakpoint_labels;
        }
      }
      $event->setBuild($build);
    }
  }

}
