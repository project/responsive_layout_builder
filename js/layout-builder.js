(function ($, Drupal) {
  Drupal.behaviors.rlbAdminBehavior = {
    attach: function (context, settings) {
      var $layoutBuilderContentPreview = $('#layout-builder-content-preview');

      var contentPreviewId = $layoutBuilderContentPreview.data('content-preview-id');

      var isContentPreview = JSON.parse(localStorage.getItem(contentPreviewId)) !== false;

      var disableContentPreview = function disableContentPreview() {

        $('[data-layout-content-preview-placeholder-label]', context).each(function (i, element) {
          var $element = $(element);

          $element.attr('data-layout-content-preview-placeholder-label');
          var breakpoint_group = $element.attr('data-breakpoint-group');
          var breakpoints_str = $element.attr('data-rlb-hide-for-breakpoint-labels');
          if (breakpoint_group && breakpoints_str) {
            var breakpoints = breakpoints_str.split(',');
            var contentPreviewHint = Drupal.theme('layoutBuilderPrependContentHint', breakpoint_group, breakpoints);
            $element.prepend(contentPreviewHint);
          }
        });
      };

      var enableContentPreview = function enableContentPreview() {
        $('.rlb-block-preview-for-hidden').remove();
      };

      $('#layout-builder-content-preview', context).on('change', function (event) {
        var isChecked = $(event.currentTarget).is(':checked');

        if (isChecked) {
          enableContentPreview();
        } else {
          disableContentPreview();
        }
      });
      if (!isContentPreview) {
        disableContentPreview();
      }
    }
  };

  Drupal.theme.layoutBuilderPrependContentHint = function (breakpoint_group, breakpoints) {
    var contentHintText = Drupal.t('Hidden for breakpoints in group "@group": @breakpoints', {'@group': breakpoint_group, '@breakpoints': breakpoints.join(', ') })
    return '<div class="rlb-block-preview rlb-block-preview-for-hidden">' + contentHintText + '</div>';
  };
})(jQuery, Drupal);
