(function ($, Drupal) {
  var loadComponents = function(breakpoints) {
    var components_data = {};
    var has_components_data = false;
    var breakpoints_processed = [];
    $.each(breakpoints, function(index, breakpoint){
      breakpoints_processed.push(breakpoint);
      $.each(drupalSettings.rlb.breakpoints[breakpoint].components, function(index2, html_element_id){
        $component = $('#' + html_element_id);
        // Ensure component with this id exists.
        // It might have loaded already and its ID might have cleared.
        if (!$component.length) {
          return;
        }

        var to_be_loaded = $component.attr('data-rlb-to-be-loaded');
        if (!(typeof to_be_loaded !== typeof undefined && to_be_loaded !== false)) {
          // This component is not for loading.
          return;
        }

        $component.find('.rlb-placeholder').after(Drupal.theme.ajaxProgressThrobber());

        components_data[$component.attr('data-component-uuid')] = {
          html_element_id: html_element_id,
          entity_id: $component.attr('data-entity-id'),
          display_id: $component.attr('data-display-id'),
          component_uuid: $component.attr('data-component-uuid')
        };
        has_components_data = true;
      });
      delete drupalSettings.rlb.breakpoints[breakpoint].components;
    });
    if (has_components_data) {
      Drupal.ajax({
        url: Drupal.url('rlb/get-components'),
        type: 'POST',
        submit: {
          components_data: components_data,
          current_path: drupalSettings.rlb.current_path
        }
      }).execute();
    }
  };
  Drupal.behaviors.rlbBehavior = {
    attach: function (context, settings) {
      $('body', context).once('rlb').each(function () {
        // Loop over all component elements requiring dynamic loading.
        $('*[data-rlb-to-be-loaded]').each(function(){
          $component = $(this);

          var html_element_id = $component.attr('id');
          // For some browsers, `attr` is undefined; for others,
          // `attr` is false.  Check for both.
          // Ensure all component elements has ID.
          if (!(typeof html_element_id !== typeof undefined && html_element_id !== false)) {
            html_element_id = 'rlb-' + $component.attr('data-component-uuid');
            $component.attr('id', html_element_id);
          }

          breakpoints = $(this).attr('data-rlb-load-for-breakpoints').split(',');
          $.each(breakpoints, function(index, breakpoint){
            if (!('components' in drupalSettings.rlb.breakpoints[breakpoint])) {
              drupalSettings.rlb.breakpoints[breakpoint]['components'] = [];
            }
            drupalSettings.rlb.breakpoints[breakpoint]['components'].push(html_element_id);
          });
        });
        var now_matching_breakpoints = [];
        $.each(drupalSettings.rlb.breakpoints, function (breakpoint_id, item){
          drupalSettings.rlb.breakpoints[breakpoint_id]['matchmedia'] =  window.matchMedia(item['mediaquery']);
          drupalSettings.rlb.breakpoints[breakpoint_id]['matchmedia'].breakpoint_id = breakpoint_id;
          if (drupalSettings.rlb.breakpoints[breakpoint_id]['matchmedia'].matches) {
            //now_matches.push(drupalSettings.rlb.breakpoints[breakpoint_id]['matchmedia']);
            now_matching_breakpoints.push(breakpoint_id);
          }
          drupalSettings.rlb.breakpoints[breakpoint_id]['matchmedia'].addListener(function(mql){
            if (mql.matches) {
              loadComponents([mql.currentTarget.breakpoint_id]);
            }
          });
        });
        loadComponents(now_matching_breakpoints);
      });
    }
  };
})(jQuery, Drupal);
